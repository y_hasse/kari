<?php
    // LINE Messaging API
    // https://developers.line.biz/ja/reference/messaging-api/

    // LINE Messaging API プッシュメッセージを送る
    $LINE_PUSH_URL = "https://api.line.me/v2/bot/message/push";

    // Channel access token (long-lived)
    // Authorization: Bearer
    // 設定例 $LINE_CHANNEL_ACCESS_TOKEN = "xxxxx略xxxxx";
    $LINE_CHANNEL_ACCESS_TOKEN = "aArn8aX9eWB0X65QO6JrD2urb5+nTx7idSm4nEZBrK2napw03KBW/R0LO+dYfa/YARbVsCZnTxg8gi+NHGy5N+xWOzSlS9RQlmqek2MqSjcrG4P3PG7mqmhMOWT7wcxE3L9K0NMk0LOY3LbAmlmqngdB04t89/1O/w1cDnyilFU=";

    // Your user ID
    // 設定例 $LINE_USER_ID = "U5xxxxx略xxxxx";
    $LINE_USER_ID = "U95be2a713cbf36e049c10b8f44165fa7";

    // 送信するメッセージ
    $message_text_1 = "PHP LINE API TEST PUSH MESSAGE";
    $message_text_2 = "Yoshihiro Hasegawa";


    // リクエストヘッダ
    $header = [
        'Authorization: Bearer ' . $LINE_CHANNEL_ACCESS_TOKEN,
        'Content-Type: application/json'
    ];

    // 送信するメッセージの下準備
    $post_values = array(
        [
        "type" => "text",
        "text" => $message_text_1
        ],
        [
        "type" => "text",
        "text" => $message_text_2
        ]
    );

    // 送信するデータ
    $post_data = [
        "to" => $LINE_USER_ID,
        "messages" => $post_values
        ];

    // デバグ確認用のログ：送信データ
    // json_encode 5.4.0 options パラメータ
    // JSON_PRETTY_PRINT
    // JSON_UNESCAPED_UNICODE
    //$file = '/tmp/post_data.txt';
    //file_put_contents($file, json_encode($post_data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE), FILE_APPEND);
    //file_put_contents($file, PHP_EOL.PHP_EOL, FILE_APPEND);

    // cURLを使った送信処理の時は true
    // file_get_contentsを使った送信処理の時は false
    $USE_CURL = true;

    if ($USE_CURL) {
        // cURLを使った送信処理
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $LINE_PUSH_URL);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($post_data));
        $result = curl_exec($curl);
        curl_close($curl);
    }
    else
    {
        // file_get_contentsを使った送信処理
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => implode("\r\n", $header),
                'content'=>  json_encode($post_data),
                'ignore_errors' => true
            )
        ));

        $result = file_get_contents(
            $LINE_PUSH_URL,
            false,
            $context
            );
    }

    // デバグ確認用のログ：受信レスポンス
    //$file = '/tmp/result.txt';
    //file_put_contents($file, $result, FILE_APPEND);
    //file_put_contents($file, PHP_EOL.PHP_EOL, FILE_APPEND);
